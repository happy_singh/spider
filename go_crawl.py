#!/usr/bin/env python

"""
Web Crawler Script.
"""

import math
import optparse
import sys
import time
from crawler import Crawler
from fetcher import FetcherClient

def parse_options():
    """
    Parses the command line args.
    """
    parser = optparse.OptionParser(usage="%prog [options] <url>", version="%prog v1.0")
    parser.add_option("-l", "--limit", type="int", default=-1, dest="limit",
            help="Number of links to fetch")
    parser.add_option("-v", "--verbose", type="int", default=0, dest="verbose",
            help="Prints all crawled and fetched links.")

    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.print_help()
        sys.exit()

    return opts, args

def print_links(links):
    padding = 1
    if len(links) > 0:
        padding = int(math.log10(len(links))) + 1
    for i, url in enumerate(links):
        log_str = "%" + str(padding) + "i. %s"
        print log_str % (i+1, url)

def main():
    opts, args = parse_options()
    url = args[0]

    print "Crawling using seed url: %s" % url
    headers = {}
    headers['User-Agent'] = "bot"
    fetcher = FetcherClient(headers)
    crawler = Crawler(url, limit=opts.limit, fetcher=fetcher)
    crawler.crawl()

    links_fetched = crawler.get_fetched_links()
    links_crawled = crawler.get_visited_links()
    print "STATS:"
    print "Given Limit: %i, Fetched links: %i, Links Crawled/Visited: %i\n\n" % (opts.limit, len(links_fetched), len(links_crawled))

    if opts.verbose:
        print "Verbose Information:"
        print "Fetched Links:"
        print_links(links_fetched)

        print "\n\nVisited/Crawled Links:"
        print_links(links_crawled)

if __name__ == "__main__":
    main()

