
# Instructions for usage:

## Commands
```
#!python

1. python go_crawl.py -h
2. python go_crawl -l 1000 -v 1 http://python.org
3. python go_crawl.py -v 1 http://python.org

```
## Note:
Capable of listening Keyboard Interrupt **Ctrl+c** and writes stats to file named **stats.txt**