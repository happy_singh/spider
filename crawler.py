import math
import urlparse
from Queue import Queue, Empty

class Crawler(object):
    """
    Web Crawler class.
    """

    _NO_LIMIT = -1
    _STATS_FILE = "stats.txt"

    def __init__(self, seed_url, limit=_NO_LIMIT, fetcher=None):
        """
        Initializes the Crawler class with seed url 
        and other optional params.
        """
        self.seed_url = seed_url
        self.limit = self.set_limit(limit)
        self._fetcher = fetcher
        self.visited = []
        self.fetched = []
        self._host = self.get_host()
        self._queue = Queue()

    def get_host(self):
        """
        Returns the seed url host.
        """
        return urlparse.urlparse(self.seed_url)[1]

    def set_fetcher(self, fetcher):
        """
        Sets the http fetcher client.
        """
        self._fetcher = fetcher

    def get_fetcher(self):
        """
        Http client getter.
        """
        return self._fetcher

    def set_limit(self, limit):
        """
        Set relevant limit on number of links to be fetched.
        """
        if limit > 0:
            return limit
        else:
            return Crawler._NO_LIMIT

    def _is_crawl_limit(self):
        """
        Check for crawl limit.
        """
        return self.limit != Crawler._NO_LIMIT

    def _is_crawl_limit_reached(self):
        """
        Check for urls remaining limit.
        """
        return len(self.fetched) >= self.limit

    def _is_url_visited(self, url):
        """
        Check for visited url/link.
        """
        return url in self.visited

    def _fetch_and_fill_queue(self, fetch_url):
        """
        Crawl given link and fills queue with fetched links.
        """
        urls = self.get_fetcher().fetch_urls(fetch_url)
        self.visited.append(fetch_url)
        self.fetched.extend(urls)
        for url in urls:
            if not self._is_url_visited(url):
                self._queue.put(url)

    def _process_queue_urls(self):
        """
        Process all urls from queue and generate stats.
        """
        while True:
            if self._is_crawl_limit() and self._is_crawl_limit_reached():
                break
            try:
                url = self._queue.get(block=False)
            except Empty:
                break
            if not self._is_url_visited(url):
                try:
                    self._fetch_and_fill_queue(url)
                except KeyboardInterrupt:
                    break
                except:
                    print "Unable to process url '%s'" % url

    def crawl(self):
        """
        Crawling initiater function.
        """
        self._queue.put(self.seed_url)
        self._process_queue_urls()
        self.write_stats()

    def get_fetched_links(self):
        """
        Returns the fetched links according to given limit.
        """
        return self.fetched[:self.limit]

    def get_visited_links(self):
        """
        Returns the visited links by http fetcher client.
        """
        return self.visited

    def write_links(self, links, file_handle):
        """
        Writes list of links with numbering.
        """
        padding = 1
        if len(links) > 0:
            padding = int(math.log10(len(links))) + 1
        log_str = "%"+str(padding)+"i. %s\n"
        for idx, link in enumerate(links):
            file_handle.write(log_str % (idx+1, unicode(link).encode('utf-8')))

    def write_stats(self):
        """
        Write stats to file named stats.
        """
        with open(Crawler._STATS_FILE, 'w') as file_handle:
            file_handle.write("STATS:\n\n")
            file_handle.write("Fetched Links:\n")
            self.write_links(self.get_fetched_links(), file_handle)
            file_handle.write("\nCrawled/Visited Links:\n")
            self.write_links(self.get_visited_links(), file_handle)
            file_handle.close()
