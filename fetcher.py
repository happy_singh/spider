import time
import urllib2
import urlparse
from BeautifulSoup import BeautifulSoup
from cgi import escape

class FetcherClient(object):
    """
    Urllib http fetcher class.
    """

    def __init__(self, headers):
        """
        Initialize fetcher client with request headers.
        """
        self.headers = headers

    def _add_headers(self, request):
        """
        Set Request header values.
        """
        for key, val in self.headers.items():
            request.add_header(key, val)

    def get_request_and_client_handle(self, url):
        """
        Get request and client handle of url.
        """
        try:
            request = urllib2.Request(url)
            client_handle = urllib2.build_opener()
        except IOError:
            return (None, None)
        return request, client_handle

    def fetch_urls(self, url):
        """
        Fetch all urls in html contents.
        Some sites are crawl protected by robots.txt
        """
        self.urls = set([])
        request, client_handle = self.get_request_and_client_handle(url)
        self._add_headers(request)
        if client_handle:
            try:
                # Don't be so harsh on servers. :)
                time.sleep(2)
                html = unicode(client_handle.open(request).read(), "utf-8",
                        errors="replace")
                soup = BeautifulSoup(html)
                a_tags = soup('a')
            except urllib2.HTTPError, e:
                print "HTTP_ERROR: http error code: %i" % e.code
            except urllib2.URLError, e:
                print "URL_ERROR: error: %s" % e
                a_tags = []

            for a_tag in a_tags:
                href = a_tag.get("href")
                if href is not None:
                    href_url = urlparse.urljoin(url, escape(href))
                    self.urls.add(href_url)
        return list(self.urls)
